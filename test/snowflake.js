'use strict';

const { Flurry } = require('../lib');

const flurry = new Flurry();

const flakes = [];

const start = process.hrtime();
for (let i = 0; i < 99999; i++) flakes.push(flurry.next());
const end = process.hrtime(start);
const nanoseconds = (end[0] * 1e9) + end[1];
const miliseconds = nanoseconds / 1e6;

const hasDuplicates = (new Set(flakes)).size !== flakes.length;
// tslint:disable-next-line:no-console
console.log(`Generated ${flakes.length} snowflakes in ${miliseconds}ms`);
// tslint:disable-next-line:no-console
console.log(hasDuplicates ? 'with duplicates' : 'with no duplicates');
