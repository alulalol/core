'use strict';

const { WebSocketServer } = require('../build');

const server = new WebSocketServer({
	host: '127.0.0.1',
	port: 27532,
	behindProxy: false,
	properties: {
		version: 'v0.0.1',
	},
});

server.on('init', () => {
	console.log('Servering initilizing...');
});

server.on('ready', (host, port) => {
	console.log(`Ready! Serving on '${host}:${port}'`);
})

server.on('clientCreate', (id, cli) => {
	console.log(`New Client has just joined the server!`, id);
});

server.on('clientReady', (id, cli) => {
	console.log(`Client successfully identified!`, id);
});

server.on('clientDelete', (id, cli) => {
	console.log(`Client disconnected!`, id);
})

server.init();