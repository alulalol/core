'use strict';

const { EmitterTools } = require('../build');

const { EventEmitter } = require('events');

const emitter = new EventEmitter();

const emitterTools = new EmitterTools();

const handleEventA = () => {
	console.log('got event a!');
};

const handleEventB = (arg1) => {
	console.log(`got event b: ${arg1}`);

	return true;
};

emitterTools.timedListener(emitter, [
	{
		eventName: 'a',
		handler: handleEventA,
	},
	{
		eventName: 'b',
		handler: handleEventB,
	},
], { timeout: 10 * 1000 }).then(() => {
	console.log('resolved!');
});


emitter.emit('a');

setTimeout(() => {
	emitter.emit('b', 'hello world');
}, 2000);