'use strict';

import { EventEmitter } from "events";

export interface ITimedListenerOptions {
	timeout?: number,
	once?: boolean,
};

export interface ITimedListenerHandler {
	eventName: string,
	handler: (...args: any[]) => boolean,
}

export class EmitterTools {
	timedListener(emitter: EventEmitter, handlers: ITimedListenerHandler[], opts?: ITimedListenerOptions) {
		return new Promise((resolve, reject) => {
			if (!emitter || !handlers) throw new Error(`'emitter', 'handlers' arguments are required.`);

			// set defaults
			opts = Object.assign({}, {
				timeout: 3 * 1000,
				once: false,
			}, opts);

			const listeners: ITimedListenerHandler[] = [];

			const handleCleanup = () => {
				// clear timer
				clearTimeout(timer);

				// remove all generated listeners
				listeners.forEach(listener => {
					emitter.removeListener(listener.eventName, listener.handler);
				});
			};

			// handles if no event handlers triggered / didn't return true
			const handleTimeout = () => {
				handleCleanup();
				return reject(new Error(`Exceeded '${opts.timeout}'ms timeout waiting for events: `));
			};

			handlers.forEach(handler => {
				const listener = (...args: any[]): boolean => {
					try {
						const result = handler.handler(...args);

						if (result === true) {
							handleCleanup();
							resolve();

							return true;
						}
					} catch (e) {
						reject(new Error(`Error while processing event: ${e}`));
						return false;
					}
				};

				emitter[opts.once ? 'once' : 'on'](handler.eventName, listener);

				listeners.push({
					eventName: handler.eventName,
					handler: listener,
				});
			});

			const timer = setTimeout(handleTimeout, opts.timeout);
		});
	}
};
