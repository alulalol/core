'use strict';

import { EventEmitter } from 'events';

import { SekitsuiMessage } from '../interfaces/messages';

export abstract class SekitsuiClient extends EventEmitter {
	public id: string;
	public properties: Map<string, any> = new Map();
	public ownerID: string = null;

	public lastHeartbeat: number = null;

	public getProperty(name: string) {
		if (!this.properties.has(name)) return null;
		return this.properties.get(name);
	}

	public setProperty(name: string, value: any) {
		this.properties.set(name, value);
	}

	public abstract sendMessage(message: SekitsuiMessage): Promise<void>;
	public abstract close(code: number, data: string): void;
}
