'use strict';

import { SekitsuiMessage } from './SekitsuiMessage';

import { SekitsuiOPCode } from '../../constants';

export interface SekitsuiReady extends SekitsuiMessage {
	op: SekitsuiOPCode.READY;
	d: {
		[key: string]: any;
	};
	t?: never;
}
