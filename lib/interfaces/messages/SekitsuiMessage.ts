'use strict';

import { SekitsuiOPCode } from '../../constants';

export interface SekitsuiMessage {
	op: SekitsuiOPCode;
	d: any;
	t?: string;
}
