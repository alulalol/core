'use strict';

import { SekitsuiOPCode } from '../../constants';
import { SekitsuiMessage } from './SekitsuiMessage';

export interface SekitsuiHello extends SekitsuiMessage {
	op: SekitsuiOPCode.HELLO;
	d: {
		id: string;
		version: string;
		heartbeatInterval: number;
		properties: {
			[key: string]: any;
		};
	};
	t?: never;
}
