'use strict';

import { SekitsuiOPCode } from '../../constants';
import { SekitsuiMessage } from './SekitsuiMessage';

export interface SekitsuiIdentify extends SekitsuiMessage {
	op: SekitsuiOPCode.IDENTIFY;
	d: {
		id?: string;
		namespace?: string;
		properties?: {
			[key: string]: any;
		};
	};
	t?: never;
}
