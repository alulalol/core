'use strict';

import { SekitsuiCollective } from '../';

export interface SekitsuiServerProperties {
	[key: string]: any;
}

export interface SekitsuiServer {
	id?: string;
	type: string;
	collective?: SekitsuiCollective;
	host?: string;
	port?: number;
	properties?: SekitsuiServerProperties;
	onLoad?(): Promise<void>;
	onUnload?(): Promise<void>;
}
