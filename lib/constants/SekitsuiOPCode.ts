'use strict';

export enum SekitsuiOPCode {
	DISPATCH = 0, // DISPATCH (server<->client)
	HELLO = 1, // HELLO (server->client)
	IDENTIFY = 2, // IDENTIFY (server<-client)
	READY = 3, // READY (server->client)
	HEARTBEAT = 4, // HEARTBEAT (server<->client)
	HEARTBEAT_ACK = 5, // HEARTBEACT_ACK (server<->client)
	// 6-9 Reserved for Sekitsui. Might be defined in the future
	// 10-19 Reserved for Sekitsui Extensions
	// 20-99 Reserved for Application use
}
