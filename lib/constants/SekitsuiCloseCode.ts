'use strict';

export enum SekitsuiCloseCode {
	UNKNOWN = 4000, // No idea whats wrong, client should try to reconnect
	DECODE_ERROR = 4001, // Client didn't send JSON / missing 'op' and/or 'd' in packet
	INVALID_OPCODE = 4002, // Client sent unknown opcode
	NOT_IDENTIFIED = 4003, // Client trying to send other opcodes prior to identifing
	IDENTIFY_TIMEOUT = 4004, // Client failed to send identify payload in time
	INVALID_IDENTIFY = 4005, // Clinet sent an invalid identify
	ALREADY_IDENTIFIED = 4006, // Client tried to identify more then once
	SESSION_TIMEOUT = 4007, // Client Session timed out. Possibly caused by not enough hearbeats
	INVALID_STATE = 4008, // Client and server appear to be out sync, sending improper payloads out of order
}
